#########################################################################################################
# Package: SubmissionHelpers ############################################################################

cmake_minimum_required(VERSION 3.5)
project( SubmissionHelpers )

# if we're living inside an ATLAS environment, we'd like to know - we don't need it though (hence the QUIET)
find_package(AnalysisBase QUIET)

# check if this is a top-level package or if we got included by another CMakeLists.txt
if(CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
  set(HAS_PARENT 0)
else()
  set(HAS_PARENT 1)
endif()

# here, we generate the setup.sh script that people can use to setup the environment for this package
set(SETUP ${CMAKE_CURRENT_BINARY_DIR}/setup.sh)
file(WRITE ${SETUP} "#!/bin/bash\n")
file(APPEND ${SETUP} "# this is an auto-generated setup script\n" )
# store the names of all the tests in SubmissionHelpersPythonTests
file(GLOB SubmissionHelpersPythonTests test/test_[A-Za-z]*.py)
# store the names of all the subpackages in SubmissionHelpersPython
file(GLOB SubmissionHelpersPython python/*.py python/submissionControllers)

# if we live inside an ATLAS environment, we have to do a bit of gymnastics
if(${AnalysisBase_FOUND})
  # this section reflects the standard ASG way of configuring CMake
  # it is executed when compiling within an ASG environment
  IF(NOT ${HAS_PARENT})
    # Also change version in setup.py and module
    atlas_project( SubmissionHelpers 1.0.0 USE AnalysisBase ${AnalysisBase_VERSION} )
    
    # Generate an environment setup script:
    lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
    install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh DESTINATION . )
    
    # Set up the usage of CPack:
    set(CMAKE_INSTALL_PREFIX /InstallArea/${ATLAS_PLATFORM})
    atlas_cpack_setup()
  ENDIF()

  atlas_subdir( SubmissionHelpers )
  find_package(Python REQUIRED)
  atlas_install_python_modules( ${SubmissionHelpersPython} )

  IF(NOT ${HAS_PARENT})
    atlas_ctest_setup() # Set up the project:
  ENDIF()
  
  find_package( GTest )

  foreach(TestScript ${SubmissionHelpersPythonTests})
    get_filename_component(TestName ${TestScript} NAME_WE)
    atlas_add_test( ${TestName} SCRIPT ${TestScript} )
    set_tests_properties(SubmissionHelpers_${TestName}_ctest PROPERTIES TIMEOUT 600) 
  endforeach()
  
  file(APPEND ${SETUP} "export PYTHONPATH=\${PYTHONPATH}:${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/python\n")
  file(APPEND ${SETUP} "export PATH=\${PATH}:${CMAKE_CURRENT_SOURCE_DIR}/share\n")
  
ELSE()
  # if we're not in ATLAS, life is much simpler

  # set the PYTHONDIR variable to contain the folder where we're going to link our python packages
  set(PYTHONDIR ${CMAKE_CURRENT_BINARY_DIR}/SubmissionHelpers)  

  # add the PYTHONPATH to the setup.sh so that our modules become importable
  file(APPEND ${SETUP} "export PYTHONPATH=\${PYTHONPATH}:${CMAKE_CURRENT_BINARY_DIR}\n")

  # add the PATH to the setup.sh, so that our executables become callable
  file(APPEND ${SETUP} "export PATH=\${PATH}:${CMAKE_CURRENT_SOURCE_DIR}/share\n")

  # link all the python files to our PYTHONPATH
  execute_process(COMMAND mkdir -p ${PYTHONDIR})
  foreach(pyfile ${SubmissionHelpersPython})
    execute_process(
      COMMAND ln -sf ${pyfile} ${PYTHONDIR}
      )
  endforeach()

  # enable ctest to allow automated running of the unit tests by CMake
  enable_testing()

  # register the test cases so that they can be called with ctest
  foreach(TestScript ${SubmissionHelpersPythonTests})
    # automatically turn the file names into the names of the unit tests
    get_filename_component(TestName ${TestScript} NAME_WE)
    # add the test to the list of tests
    add_test( NAME ${TestName} COMMAND ${TestScript})
    # set a reasonable timeout for our tests - some of them take a bit longer
    set_tests_properties(${TestName} PROPERTIES TIMEOUT 600) 
  endforeach()
ENDIF()
