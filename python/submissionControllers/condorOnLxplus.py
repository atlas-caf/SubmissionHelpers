
import os.path
import time
import subprocess
from SubmissionHelpers.submissionControllerBase import submissionControllerBase,getCmdAdditions
from SubmissionHelpers.common import mkdir_p
from SubmissionHelpers.task import task,taskStatus
from SubmissionHelpers.submissionControllers.condor import CondorController


class CondorControllerOnLxplus(CondorController):
  controllerPriority = 2 # choose 2 to prioritize over condorController
  """
  Implementation of a submission controller using the slurm scheduler.
  """

  def _apply_base_command(self, args, thisTask, cmd_args, payload, setup):
    """
    Appends 'condor_submit' to the command line arguments list in-place and returns
    the original payload string. The method is called by __submitSingleTask()
    to build command line arguments and the payload.
    """

    mkdir_p(self.workloadDir)
    #not human readable but at least a safe file name which is unique as long as the tasks identifier is unique:
    import hashlib
    hash_object = hashlib.md5(thisTask.identifier.encode("ascii"))
    identifier = hash_object.hexdigest()
    self.payloadScriptName = os.path.join( self.workloadDir, identifier )
    self.submitScriptName = self.payloadScriptName + "_submitScript.sh"
    self.wrapperScriptName = self.payloadScriptName + "_wrapper.sh"
    self.payloadScriptName += ".sh"
    filesToDelete = [self.submitScriptName, self.wrapperScriptName, self.payloadScriptName]

    self.wrapperScript = self._getWrapperScript(self.submitScriptName, filesToDelete)

    # automatically write the submit script for container submission
    writeSubmissionScript = ["batchScript", self.payloadScriptName, "-O", self.submitScriptName]
    process = subprocess.Popen(writeSubmissionScript, stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               universal_newlines=True)
    (stdout, stderr) = process.communicate()
    try:
      (stdout, stderr) = (stdout.decode('ascii'), stderr.decode('ascii'))
    except AttributeError:
      pass
    status_code = process.returncode
    if status_code != 0:
      raise RuntimeError('Failed to execute \'batchScript\' - are you running in a batch environment? e.g., did you launch the ATLAS container using \'setupATLAS -c centos7+batch\'?')

    cmd_args.append("condor_submit")
    cmd_args.append("--terse") #machine readable stdout
    cmd_args.extend(getCmdAdditions()) #add optionally some more arguments
    cmd_args.append("-") #read from stdin
    cmd_args.append("-queue") #this replaces adding a queue statement in the "payload"
    cmd_args.append("1") #number of copies of the job to submit
    #prepare payload scripts. Note: this should be the last argument

    setup[:] = [] #the setup instructions are written to a file for htcondor, hence, don't pipe them to the submission binary!
    #create pseudo-payload, that is what is fed to condor_submit via stdin
    payload[:] = []
    payload.append("executable = /bin/bash")
    payload.append("arguments = {wrapper}".format(wrapper=os.path.abspath(self.wrapperScriptName)))
    # don't copy the bash binary from the submission node (maybe we could actually use this to copy/spool our payload and delete it right away?
    payload.append("transfer_executable = false")
    # payload.append("requirements = TARGET.OpSysMajorVer == 7") # MJB: don't require Centos7, as lxplus uses AlmaLinux9 now
    #payload.append("initialdir = {:s}".format(os.getcwd()))
    if 'X509_USER_PROXY' in os.environ and os.path.isfile(os.environ['X509_USER_PROXY']) and os.system('voms-proxy-info -e &> /dev/null') == 0:
      payload.append("x509userproxy = $ENV(X509_USER_PROXY)")
    return True

  def _execute_submission_command(self, thisTask, cmd_args, payload, setup):
    """
    Communicate the task to the batch system.
    """
    setupAndPayload = "\n".join(thisTask.setup+thisTask.payload)

    mkdir_p(self.workloadDir)
    with open(self.payloadScriptName, "w") as f:
      f.write(setupAndPayload)
    with open(self.wrapperScriptName, "w") as f:
      f.write(self.wrapperScript)
    os.chmod(self.payloadScriptName, 0o0744)
    os.chmod(self.wrapperScriptName, 0o0744)

    cmd_args += sum([['-a', '\"%s\"' % app] for app in (setup + payload)], [])
    process = subprocess.Popen(cmd_args, stdin=subprocess.PIPE,
                               stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               universal_newlines=True)

    (stdout, stderr) = process.communicate()
    try:
      (stdout, stderr) = (stdout.decode('ascii'), stderr.decode('ascii'))
    except AttributeError:
      pass
    status_code = process.returncode

    return status_code, stdout, stderr

  @staticmethod
  def _is_available():
    """
    This method returns True if the batch system corresponding to the particular
    controller appears to be available (the exact conditions can vary), typically
    this implies a check for the submission binary being available. If the
    corresponding batch system is not available, False is returned.

    This method must be implemented be a concrete submission controller class.

    Implementation for condor_submit: checks availability of "condor_submit" binary
    using "which condor_submit" and checking the return code of this call.
    """
    p = subprocess.Popen("which condor_submit".split(), stdin=subprocess.PIPE,stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    (stdout, stderr) = p.communicate("")
    try:
      (stdout, stderr) = (stdout.decode('ascii'), stderr.decode('ascii'))
    except AttributeError:
      pass

    success = not p.returncode #returncode is 0 if everything went fine!
    return success and stdout.strip("\n") == "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/wrappers/containers/condor/condor_submit"
